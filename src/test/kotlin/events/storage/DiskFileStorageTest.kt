package events.storage

import events.dto.file.SaveFileDTO
import io.micronaut.test.extensions.junit5.annotation.MicronautTest
import jakarta.inject.Inject
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.io.ByteArrayInputStream
import java.nio.file.Files
import java.nio.file.Paths

@MicronautTest
class DiskFileStorageTest {
    @Inject
    lateinit var fileStorage: DiskFileStorage

    private val uploadsDir = Paths.get(DiskFileStorage.UPLOAD_DIR)

    @BeforeEach
    fun setup() {
        // Setup code if needed
    }

    @AfterEach
    fun cleanup() {
        // Clean up files and directories created by tests
        if (Files.exists(uploadsDir)) {
            Files.walk(uploadsDir)
                .sorted(Comparator.reverseOrder())
                .forEach(Files::delete)
        }
    }


    // Can successfully upload a file to disk
    @Test
    fun test_upload_file_successfully() {
        // Arrange
        val fileName = "test"
        val fileExtension = "txt"
        val inputStream = ByteArrayInputStream("Test content".toByteArray())
        val saveFileDTO = SaveFileDTO(fileName, fileExtension, inputStream)

        // Act
        fileStorage.uploadFile(saveFileDTO)

        // Assert
        val targetPath = Paths.get("${DiskFileStorage.UPLOAD_DIR}/$fileName.$fileExtension")
        assertTrue(Files.exists(targetPath))
    }
}
