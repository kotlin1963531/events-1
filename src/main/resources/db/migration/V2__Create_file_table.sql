CREATE TABLE files (
                       id UUID PRIMARY KEY,
                       original_name VARCHAR(255) NOT NULL,
                       extension VARCHAR(255) NOT NULL,
                       content_type VARCHAR(255) NOT NULL,
                       size BIGINT NOT NULL,
                       created_at TIMESTAMP NOT NULL,
                       updated_at TIMESTAMP NOT NULL
);
