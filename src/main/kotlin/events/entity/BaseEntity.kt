package events.entity

import jakarta.persistence.*
import java.util.Date


@MappedSuperclass
abstract class BaseEntity {

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_at", nullable = false, updatable = false)
    var createdAt: Date? = null

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_at", nullable = false)
    var updatedAt: Date? = null

    @PrePersist
    protected fun onCreate() {
        createdAt = Date()
        updatedAt = createdAt
    }

    @PreUpdate
    protected fun onUpdate() {
        updatedAt = Date()
    }
}

