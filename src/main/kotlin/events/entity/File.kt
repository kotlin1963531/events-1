package events.entity

import io.micronaut.serde.annotation.Serdeable
import jakarta.persistence.*
import jakarta.validation.constraints.NotBlank
import jakarta.validation.constraints.NotNull
import java.util.UUID

@Entity
@Table(name = "files")
@Serdeable
data class File(
    @Id
    var id: UUID,

    @Column(name = "original_name", nullable = false)
    @field:NotBlank
    @field:NotNull
    var originalName: String,

    @Column(name = "extension", nullable = false)
    @field:NotBlank
    @field:NotNull
    var extension: String,

    @Column(name= "content_type", nullable = false)
    @field:NotBlank
    @field:NotNull
    var contentType: String,

    @Column(name= "size", nullable = false)
    @field:NotNull
    var size: Long,
) : BaseEntity()
