package events.storage

import events.dto.file.SaveFileDTO
import events.exception.FolderCreationException
import jakarta.inject.Singleton
import org.slf4j.LoggerFactory
import java.io.IOException
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.StandardCopyOption

@Singleton
class DiskFileStorage : FileStorage {
    private val logger = LoggerFactory.getLogger(DiskFileStorage::class.java)
    companion object {
        const val UPLOAD_DIR: String = "uploads"
    }

    override fun uploadFile(file: SaveFileDTO) {
        val targetDir = checkForUploadDir()

        val targetPath = targetDir.resolve("${file.fileName}.${file.fileExtension}")
        val inputStream = file.inputStream

        // Save file to disk
        try {
            Files.copy(inputStream, targetPath, StandardCopyOption.REPLACE_EXISTING)
        } catch (e: Exception) {
            logger.error("Failed to upload file ${file.fileName} to disk.")
            logger.error(e.toString())
        }
    }

    private fun checkForUploadDir(): Path {
        val targetDir = Paths.get(UPLOAD_DIR)
        if (!Files.exists(targetDir)) {
            try {
                Files.createDirectories(targetDir)
            } catch (e: IOException) {
                logger.error("Failed to create the directory: $targetDir", e)
                throw FolderCreationException()
            }
        }

        return targetDir
    }
}
