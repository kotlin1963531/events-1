package events.storage

import events.dto.file.SaveFileDTO

interface FileStorage {
    fun uploadFile(file: SaveFileDTO)
}
