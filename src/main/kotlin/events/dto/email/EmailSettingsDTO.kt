package events.dto.email

import jakarta.validation.constraints.Email
import jakarta.validation.constraints.NotBlank
import jakarta.validation.constraints.NotNull

data class EmailSettingsDTO(
    @field:NotNull @field:NotBlank @field:Email
    val recipient: String,
    @field:NotNull @field:NotBlank @field:Email
    val sender: String,
)
