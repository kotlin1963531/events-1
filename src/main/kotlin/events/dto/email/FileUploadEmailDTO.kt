package events.dto.email

import java.util.Date

data class FileUploadEmailDTO (
    val fileName: String,
    val size: Long,
    val createdAt: Date,
)
