package events.dto.file

import java.io.InputStream

data class SaveFileDTO(
    val fileName: String,
    val fileExtension: String,
    val inputStream: InputStream,
)
