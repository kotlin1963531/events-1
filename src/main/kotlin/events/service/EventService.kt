package events.service

import events.entity.Event
import events.repository.EventRepository
import jakarta.inject.Inject
import jakarta.inject.Singleton

@Singleton
class EventService {

    @Inject
    private lateinit var eventRepository: EventRepository;

    fun createEvent(createEvent: Event): Event {
        return eventRepository.save(createEvent);
    }
}
