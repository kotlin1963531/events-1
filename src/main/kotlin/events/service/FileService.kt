package events.service

import events.dto.file.SaveFileDTO
import events.entity.File
import events.producer.FileUploadProducer
import events.repository.FileRepository
import events.storage.FileStorage
import io.micronaut.http.multipart.CompletedFileUpload
import jakarta.inject.Inject
import jakarta.inject.Singleton
import org.slf4j.LoggerFactory
import java.util.*

@Singleton
class FileService {
    @Inject
    private lateinit var fileRepository: FileRepository

    @Inject
    private lateinit var fileStorage: FileStorage

    @Inject
    private lateinit var fileUploadProducer: FileUploadProducer

    private val logger = LoggerFactory.getLogger(FileService::class.java)

    fun saveFile(fileToUpload: CompletedFileUpload): File {
        val file: File = File(
            UUID.randomUUID(),
            fileToUpload.filename,
            fileToUpload.contentType.get().extension,
            fileToUpload.contentType.get().name,
            fileToUpload.size
        )

        fileRepository.save(file)
        fileStorage.uploadFile(
            SaveFileDTO(
                file.id.toString(),
                file.extension,
                fileToUpload.inputStream
            )
        )

        fileUploadProducer.send(file)
        return file
    }
}
