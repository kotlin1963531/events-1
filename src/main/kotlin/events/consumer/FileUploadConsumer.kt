package events.consumer

import events.dto.email.EmailSettingsDTO
import events.dto.email.FileUploadEmailDTO
import events.email.FileUploadEmail
import events.entity.File
import example.micronaut.FilePoolListener
import io.micronaut.rabbitmq.annotation.Queue
import io.micronaut.rabbitmq.annotation.RabbitListener
import jakarta.inject.Inject
import java.util.*

@RabbitListener
class FileUploadConsumer {

    @Inject
    private lateinit var fileUploadEmail: FileUploadEmail

    @Queue(FilePoolListener.QUEUE_NAME)
    fun sendUploadFileEmail(file: File) {
        fileUploadEmail.sendEmail(
            EmailSettingsDTO(
                "recipient@test.cz",
                "sender@test.cz",
            ), FileUploadEmailDTO(file.originalName, file.size, file.createdAt as Date)
        )
    }
}
