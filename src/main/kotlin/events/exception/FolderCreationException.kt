package events.exception


class FolderCreationException(message: String = "Cannot create folder", cause: Throwable? = null) : Exception(message, cause)

