package events.email

import events.dto.email.EmailSettingsDTO
import events.dto.email.FileUploadEmailDTO
import jakarta.inject.Singleton

@Singleton
class FileUploadEmail : BaseEmail<FileUploadEmailDTO>() {

    override fun sendEmail(emailSettingsDTO: EmailSettingsDTO, dto: FileUploadEmailDTO) {
        emailSender.send(
            buildEmail(emailSettingsDTO)
                .subject("New file uploaded")
                .body("""
                        New file ${dto.fileName} with size ${dto.size} has been uploaded at ${dto.createdAt.toString()}
                    """)
        )
    }
}
