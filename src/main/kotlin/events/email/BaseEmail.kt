package events.email

import events.dto.email.EmailSettingsDTO
import io.micronaut.context.annotation.Bean
import io.micronaut.email.Email
import io.micronaut.email.EmailSender
import jakarta.inject.Inject
import jakarta.inject.Singleton
import org.slf4j.Logger
import org.slf4j.LoggerFactory

@Singleton
abstract class BaseEmail<DTO> {
    @Inject
    protected lateinit var emailSender: EmailSender<Any, Any>

    protected val logger: Logger = LoggerFactory.getLogger(this::class.java)

    abstract fun sendEmail(emailSettingsDTO: EmailSettingsDTO, dto: DTO)

    protected fun buildEmail(dto: EmailSettingsDTO): Email.Builder {
        return Email.builder()
            .to(dto.recipient)
            .from(dto.sender)
    }
}
