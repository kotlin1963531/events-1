package events.controller

import events.dto.email.EmailSettingsDTO
import events.dto.email.FileUploadEmailDTO
import events.email.FileUploadEmail
import events.entity.Event
import events.entity.File
import events.service.EventService
import events.service.FileService
import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Post
import io.micronaut.http.multipart.CompletedFileUpload
import jakarta.inject.Inject

@Controller("/event")
class EventController {

    @Inject
    private lateinit var eventService: EventService;

    @Inject
    private lateinit var fileService: FileService

    @Post(uri = "/", produces = ["application/json"])
    fun createEvent(@Body event: Event): Event {
        return eventService.createEvent(event)
    }

    @Post(uri = "/file", consumes = ["multipart/form-data"], produces = ["application/json"])
    fun uploadFile(file: CompletedFileUpload): File {
        return fileService.saveFile(file)
    }
}
