package events.producer

import events.entity.File
import example.micronaut.FilePoolListener
import io.micronaut.rabbitmq.annotation.Queue
import io.micronaut.rabbitmq.annotation.RabbitClient

@RabbitClient(FilePoolListener.EXCHANGE_NAME)
interface FileUploadProducer {
    @Queue(FilePoolListener.QUEUE_NAME)
    fun send(file: File)
}
