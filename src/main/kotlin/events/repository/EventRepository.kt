package events.repository

import events.entity.Event
import io.micronaut.data.annotation.Repository
import io.micronaut.data.repository.CrudRepository

@Repository
interface EventRepository: CrudRepository<Event, Long> {

}
