package events.repository

import events.entity.File
import io.micronaut.data.annotation.Repository
import io.micronaut.data.jpa.repository.JpaRepository
import java.util.UUID

@Repository
interface FileRepository: JpaRepository<File, UUID> {

}
